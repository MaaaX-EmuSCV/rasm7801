; TESTASM 7801
; Test code for EPOCH/YENO SuperCassette Vision
; Author: Maxime MARCONATO
;
; ALL NEC uPD7801 OPCODES AND RAM7801 DIRECTIVES
;

ORG #8000
ALIGN 256,#00
LIMIT #8BFF
PROTECT #8C00,#8CFF

LBL_START:
    HLT
    NOP
    INX SP
    INX B
    INX D
    INX H
    NOP 3
    DCX SP
    DCX B
    DCX D
    DCX H
    LXI SP,#0123
    LXI B,#4567
    LXI D,#89AB
    LXI H,#CDEF
    ANIW V.#12,#34
    ANI V,#01
    ANI A,#02
    ANI1 A,#12
    ANI2 A,#22
    ANI B,#03
    ANI C,#04
    ANI D,#05
    ANI E,#06
    ANI H,#07
    ANI L,#08
    ANI PA,#09
    ANI PB,#0A
    ANI PC,#0B
    ANI MK,#0C
LBL_RET:
    RET
    SIO
    MOV A,B
    MOV A,C
    MOV A,D
    MOV A,E
    MOV A,H
    MOV A,L
    MOV B,A
    MOV C,A
    MOV D,A
    MOV E,A
    MOV H,A
    MOV L,A
    MOV A,PA
    MOV A,PB
    MOV A,PC
    MOV A,MK
    MOV A,MB
    MOV A,MC
    MOV A,TM0
    MOV A,TM1
    MOV A,S
    MOV PA,A
    MOV PB,A
    MOV PC,A
    MOV MK,A
    MOV MB,A
    MOV MC,A
    MOV TM0,A
    MOV TM1,A
    MOV S,A
    MOV V,#0102
    MOV A,#0304
    MOV B,#0506
    MOV C,#0708
    MOV D,#090A
    MOV E,#0B0C
    MOV H,#0D0E
    MOV L,#0F10
    MOV #FFF8,V
    MOV #FFF9,A
    MOV #FFFA,B
    MOV #FFFB,C
    MOV #FFFC,D
    MOV #FFFD,E
    MOV #FFFE,H
    MOV #FFFF,L
    EX
    EXX
    ORIW V.#12,#34
    ORI V,#01
    ORI A,#02
    ORI1 A,#12
    ORI2 A,#22
    ORI B,#03
    ORI C,#04
    ORI D,#05
    ORI E,#06
    ORI H,#07
    ORI L,#08
    ORI PA,#09
    ORI PB,#0A
    ORI PC,#0B
    ORI MK,#0C
    XRI V,#01
    XRI A,#02
    XRI1 A,#12
    XRI2 A,#23
    XRI B,#03
    XRI C,#04
    XRI D,#05
    XRI E,#06
    XRI H,#07
    XRI L,#08
    XRI PA,#09
    XRI PB,#0A
    XRI PC,#0B
    XRI MK,#0C
    RETS
    STM
    INRW V.#12
    TABLE
    GTIW V.#12,#34
    ADINC V,#01
    ADINC A,#02
    ADINC1 A,#12
    ADINC2 A,#22
    ADINC B,#03
    ADINC C,#04
    ADINC D,#05
    ADINC E,#06
    ADINC H,#07
    ADINC L,#08
    ADINC PA,#09
    ADINC PB,#0A
    ADINC PC,#0B
    ADINC MK,#0C
    GTI V,#01
    GTI A,#02
    GTI1 A,#12
    GTI2 A,#22
    GTI B,#03
    GTI C,#04
    GTI D,#05
    GTI E,#06
    GTI H,#07
    GTI L,#08
    GTI PA,#09
    GTI PB,#0A
    GTI PC,#0B
    GTI MK,#0C
    LDAW V.#FD
    LDAX B
    LDAX D
    LDAX H
    LDAX D+
    LDAX H+
    LDAX D-
    LDAX H-
    DCRW V.#12
    BLOCK
    LTIW V.#12,#34
    SUINB V,#01
    SUINB A,#02
    SUINB1 A,#12
    SUINB2 A,#23
    SUINB B,#03
    SUINB C,#04
    SUINB D,#05
    SUINB E,#06
    SUINB H,#07
    SUINB L,#08
    SUINB PA,#09
    SUINB PB,#0A
    SUINB PC,#0B
    SUINB MK,#0C
    LTI V,#01
    LTI A,#02
    LTI1 A,#12
    LTI2 A,#23
    LTI B,#03
    LTI C,#04
    LTI D,#05
    LTI E,#06
    LTI H,#07
    LTI L,#08
    LTI PA,#09
    LTI PB,#0A
    LTI PC,#0B
    LTI MK,#0C
    STAW V.#01
    STAX B
    STAX D
    STAX H
    STAX D+
    STAX H+
    STAX D-
    STAX H-
    INR A
    INR B
    INR C
    CALL LBL_RET
    CALL $+3
    ONIW V.1,2
    ADI V,#01
    ADI A,#02
    ADI1 A,#12
    ADI2 A,#22
    ADI B,#03
    ADI C,#04
    ADI D,#05
    ADI E,#06
    ADI H,#07
    ADI L,#08
    ADI PA,#09
    ADI PB,#0A
    ADI PC,#0B
    ADI MK,#0C
    ONI V,#01
    ONI A,#02
    ONI1 A,#12
    ONI2 A,#22
    ONI B,#03
    ONI C,#04
    ONI D,#05
    ONI E,#06
    ONI H,#07
    ONI L,#08
    ONI PA,#09
    ONI PB,#0A
    ONI PC,#0B
    ONI MK,#0C
    SKIT F0
    SKIT FT
    SKIT F1
    SKIT F2
    SKIT FS
    SKC
    SKZ
    PUSH V
    PUSH B
    PUSH D
    PUSH H
    POP V
    POP B
    POP D
    POP H
    SKNIT F0
    SKNIT FT
    SKNIT F1
    SKNIT F2
    SKNIT FS
    SKNC
    SKNZ
    EI
    DI
    CLC
    STC
    PEN
    PEX
    RAL
    RAR
    RCL
    RCR
    SHAL
    SHAR
    SHCL
    SHCR
    RLD
    RRD
    PER
    MVIX B,#01
    MVIX D,#02
    MVIX H,#03
    IN #00
    IN #BF
    OUT #00
    OUT #BF
    JRE $-254
    JRE $+1
    JRE $+2
    JRE $+257
    JRE LBL_JRE1
LBL_JRE1:
    JRE LBL_JRE2
    NOP 255
LBL_JRE2:
    NOP 254
    JRE LBL_JRE2
LBL_JRE3:
    JRE LBL_JRE3+1
    DCR A
    DCR B
    DCR C
    JMP #8000
LBL_JMP1:
    JMP LBL_JMP1-1
    JMP LBL_JMP1
    JMP LBL_JMP1+1
    JMP $-1
    JMP $
    JMP $+1
    OFFIW V.#01,02
    ACI V,#01
    ACI A,#02
    ACI1 A,#12
    ACI2 A,#22
    ACI B,#03
    ACI C,#04
    ACI D,#05
    ACI E,#06
    ACI H,#07
    ACI L,#08
    ACI PA,#09
    ACI PB,#0A
    ACI PC,#0B
    ACI MK,#0C
    OFFI V,#01
    OFFI A,#02
    OFFI1 A,#12
    OFFI2 A,#22
    OFFI B,#03
    OFFI C,#04
    OFFI D,#05
    OFFI E,#06
    OFFI H,#07
    OFFI L,#08
    OFFI PA,#09
    OFFI PB,#0A
    OFFI PC,#0B
    OFFI MK,#0C
    BIT 0,V.0
    BIT 1,V.1
    BIT 2,V.2
    BIT 3,V.3
    BIT 4,V.4
    BIT 5,V.5
    BIT 6,V.6
    BIT 7,V.7
    ANA V,A
    ANA2 A,A
    ANA B,A
    ANA C,A
    ANA D,A
    ANA E,A
    ANA H,A
    ANA L,A
    ANA A,V
    ANA A,A
    ANA1 A,A
    ANA A,B
    ANA A,C
    ANA A,D
    ANA A,E
    ANA A,H
    ANA A,L
    XRA V,A
    XRA2 A,A
    XRA B,A
    XRA C,A
    XRA D,A
    XRA E,A
    XRA H,A
    XRA L,A
    XRA A,V
    XRA A,A
    XRA1 A,A
    XRA A,B
    XRA A,C
    XRA A,D
    XRA A,E
    XRA A,H
    XRA A,L
    ORA V,A
    ORA2 A,A
    ORA B,A
    ORA C,A
    ORA D,A
    ORA E,A
    ORA H,A
    ORA L,A
    ORA A,V
    ORA A,A
    ORA1 A,A
    ORA A,B
    ORA A,C
    ORA A,D
    ORA A,E
    ORA A,H
    ORA A,L
    ADDNC V,A
    ADDNC2 A,A
    ADDNC B,A
    ADDNC C,A
    ADDNC D,A
    ADDNC E,A
    ADDNC H,A
    ADDNC L,A
    ADDNC A,V
    ADDNC A,A
    ADDNC1 A,A
    ADDNC A,B
    ADDNC A,C
    ADDNC A,D
    ADDNC A,E
    ADDNC A,H
    ADDNC A,L
    GTA V,A
    GTA2 A,A
    GTA B,A
    GTA C,A
    GTA D,A
    GTA E,A
    GTA H,A
    GTA L,A
    GTA A,V
    GTA A,A
    GTA1 A,A
    GTA A,B
    GTA A,C
    GTA A,D
    GTA A,E
    GTA A,H
    GTA A,L
    SUBNB V,A
    SUBNB2 A,A
    SUBNB B,A
    SUBNB C,A
    SUBNB D,A
    SUBNB E,A
    SUBNB H,A
    SUBNB L,A
    SUBNB A,V
    SUBNB A,A
    SUBNB1 A,A
    SUBNB A,B
    SUBNB A,C
    SUBNB A,D
    SUBNB A,E
    SUBNB A,H
    SUBNB A,L
    LTA V,A
    LTA2 A,A
    LTA B,A
    LTA C,A
    LTA D,A
    LTA E,A
    LTA H,A
    LTA L,A
    LTA A,V
    LTA A,A
    LTA1 A,A
    LTA A,B
    LTA A,C
    LTA A,D
    LTA A,E
    LTA A,H
    LTA A,L
    ADD V,A
    ADD2 A,A
    ADD B,A
    ADD C,A
    ADD D,A
    ADD E,A
    ADD H,A
    ADD L,A
    ADD A,V
    ADD A,A
    ADD1 A,A
    ADD A,B
    ADD A,C
    ADD A,D
    ADD A,E
    ADD A,H
    ADD A,L
    ADC V,A
    ADC2 A,A
    ADC B,A
    ADC C,A
    ADC D,A
    ADC E,A
    ADC H,A
    ADC L,A
    ADC A,V
    ADC A,A
    ADC1 A,A
    ADC A,B
    ADC A,C
    ADC A,D
    ADC A,E
    ADC A,H
    ADC A,L
    SUB V,A
    SUB2 A,A
    SUB B,A
    SUB C,A
    SUB D,A
    SUB E,A
    SUB H,A
    SUB L,A
    SUB A,V
    SUB A,A
    SUB1 A,A
    SUB A,B
    SUB A,C
    SUB A,D
    SUB A,E
    SUB A,H
    SUB A,L
    NEA V,A
    NEA2 A,A
    NEA B,A
    NEA C,A
    NEA D,A
    NEA E,A
    NEA H,A
    NEA L,A
    NEA A,V
    NEA A,A
    NEA1 A,A
    NEA A,B
    NEA A,C
    NEA A,D
    NEA A,E
    NEA A,H
    NEA A,L
    SBB V,A
    SBB2 A,A
    SBB B,A
    SBB C,A
    SBB D,A
    SBB E,A
    SBB H,A
    SBB L,A
    SBB A,V
    SBB A,A
    SBB1 A,A
    SBB A,B
    SBB A,C
    SBB A,D
    SBB A,E
    SBB A,H
    SBB A,L
    EQA V,A
    EQA2 A,A
    EQA B,A
    EQA C,A
    EQA D,A
    EQA E,A
    EQA H,A
    EQA L,A
    EQA A,V
    EQA A,A
    EQA1 A,A
    EQA A,B
    EQA A,C
    EQA A,D
    EQA A,E
    EQA A,H
    EQA A,L
    ONA A,V
    ONA A,A
    ONA A,B
    ONA A,C
    ONA A,D
    ONA A,E
    ONA A,H
    ONA A,L
    OFFA A,V
    OFFA A,A
    OFFA A,B
    OFFA A,C
    OFFA A,D
    OFFA A,E
    OFFA A,H
    OFFA A,L
    DAA
    RETI
    CALB
    SUI V,#01
    SUI A,#02
    SUI1 A,#12
    SUI2 A,#22
    SUI B,#03
    SUI C,#04
    SUI D,#05
    SUI E,#06
    SUI H,#07
    SUI L,#08
    SUI PA,#09
    SUI PB,#0A
    SUI PC,#0B
    SUI MK,#0C
    NEI V,#01
    NEI A,#02
    NEI1 A,#12
    NEI2 A,#22
    NEI B,#03
    NEI C,#04
    NEI D,#05
    NEI E,#06
    NEI H,#07
    NEI L,#08
    NEI PA,#09
    NEI PB,#0A
    NEI PC,#0B
    NEI MK,#0C
    SBI V,#01
    SBI A,#02
    SBI1 A,#12
    SBI2 A,#22
    SBI B,#03
    SBI C,#04
    SBI D,#05
    SBI E,#06
    SBI H,#07
    SBI L,#08
    SBI PA,#09
    SBI PB,#0A
    SBI PC,#0B
    SBI MK,#0C
    EQI V,#01
    EQI A,#02
    EQI1 A,#12
    EQI2 A,#22
    EQI B,#03
    EQI C,#04
    EQI D,#05
    EQI E,#06
    EQI H,#07
    EQI L,#08
    EQI PA,#09
    EQI PB,#0A
    EQI PC,#0B
    EQI MK,#0C
    NEIW V.#12,#34
    MVI V,#01
    MVI A,#02
    MVI B,#03
    MVI C,#04
    MVI D,#05
    MVI E,#06
    MVI H,#07
    MVI L,#08
    SSPD #0123
    LSPD #4567
    SBCD #89AB
    LBCD #CDEF
    SDED #0123
    LDED #4567
    SHLD #89AB
    LHLD #CDEF
    ANAX B
    ANAX D
    ANAX H
    ANAX D+
    ANAX H+
    ANAX D-
    ANAX H-
    XRAX B
    XRAX D
    XRAX H
    XRAX D+
    XRAX H+
    XRAX D-
    XRAX H-
    ORAX B
    ORAX D
    ORAX H
    ORAX D+
    ORAX H+
    ORAX D-
    ORAX H-
    ADDNCX B
    ADDNCX D
    ADDNCX H
    ADDNCX D+
    ADDNCX H+
    ADDNCX D-
    ADDNCX H-
    GTAX B
    GTAX D
    GTAX H
    GTAX D+
    GTAX H+
    GTAX D-
    GTAX H-
    SUBNBX B
    SUBNBX D
    SUBNBX H
    SUBNBX D+
    SUBNBX H+
    SUBNBX D-
    SUBNBX H-
    LTAX B
    LTAX D
    LTAX H
    LTAX D+
    LTAX H+
    LTAX D-
    LTAX H-
    ADDX B
    ADDX D
    ADDX H
    ADDX D+
    ADDX H+
    ADDX D-
    ADDX H-
    ONAX B
    ONAX D
    ONAX H
    ONAX D+
    ONAX H+
    ONAX D-
    ONAX H-
    ADCX B
    ADCX D
    ADCX H
    ADCX D+
    ADCX H+
    ADCX D-
    ADCX H-
    OFFAX B
    OFFAX D
    OFFAX H
    OFFAX D+
    OFFAX H+
    OFFAX D-
    OFFAX H-
    SUBX B
    SUBX D
    SUBX H
    SUBX D+
    SUBX H+
    SUBX D-
    SUBX H-
    NEAX B
    NEAX D
    NEAX H
    NEAX D+
    NEAX H+
    NEAX D-
    NEAX H-
    SBBX B
    SBBX D
    SBBX H
    SBBX D+
    SBBX H+
    SBBX D-
    SBBX H-
    EQAX B
    EQAX D
    EQAX H
    EQAX D+
    EQAX H+
    EQAX D-
    EQAX H-
    MVIW V.#12,#34
    SOFTI
    JB
    ANAW V.#01
    XRAW V.#01
    ORAW V.#01
    ADDNCW V.#01
    GTAW V.#01
    SUBNBW V.#01
    LTAW V.#01
    ADDW V.#01
    ONAW V.#01
    ADCW V.#01
    OFFAW V.#01
    SUBW V.#01
    NEAW V.#01
    SBBW V.#01
    EQAW V.#01
    EQIW V.#12,#34
    CALF #800
    CALF #FFF
    CALT #80
    CALT #FE
    JR LBL_JRP1
LBL_JRP1:
    JR LBL_JRP2
    NOP
LBL_JRP2:
    JR LBL_JRP32
    NOP 31
LBL_JRP32:
LBL_JRM31:
    NOP 31
    JR LBL_JRM31
LBL_JRM1:
    NOP
    JR LBL_JRM1
    JR LBL_JRM1+1
LBL_JR0:
    JR LBL_JR0
    JR $
    JR $-1
    JR $+1

LBL_DATA:
    DEFB #01, #02, #03, #04
    DB #05, #06, #07, #08
    DB 'abcd'
    DEFW #090A, #0B0C
    DW #0D0E, #0F10
    DEFI #11121314
    DEFS 4,#15
    DS 8
    DS 4,#16, 4,#17, 4,#18
    DS 2,'ABCD'
    STRUCT ST1
        CH1 DEFB #01, #02, #03, #04
        CH2 DB #05, #06, #07, #08
        CH3 DB 'abcd'
        CH4 DEFW #090A, #0B0C
        CH5 DW #0D0E, #0F10
        CH6 DEFI #11121314
        CH7 DEFS 4,#15
        DS 8
        DS 4,#16, 4,#17, 4,#18
        DS 2,'ABCD'
    ENDSTRUCT
    STRUCT ST1 MYST1
    DEFB 0,0,'dt',0
    STR 'dt'

    DEFB 'dT',0

    CHARSET 'T','t'
    DEFB 'dT',0

    CHARSET #44,'d'
    DEFB 'DT',0
    DEFB 'abcdeABCDE'

    CHARSET 'A','Z','a'
    DEFB 'abcdeABCDE'

    CHARSET
    DEFB 'abcdeABCDE',0

    DEFW $,$

    tab1 EQU #8000
    tab2 EQU tab1+#100
    CALL tab2


    myvar1=#11
    LET myvar2=#22
    adi a,myvar1
    adi b,myvar2