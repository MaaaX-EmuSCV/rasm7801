;
; HELLO WORLD
; The first true program compiled with RASM7801
;

ORG #8000
    DB #48  // Cartidge ROM signature header

    ; Program start
    DI
    LXI	SP,#FFD2
    EI

    ; Clear Text All Area +
   	CALT   #8C

    ; Set VDC
	LXI	H,VDC0      ; Values
	LXI	D,#3400     ; VDC address
	MVI	C,#03       ; 4 bytes to copy (VDC0, VDC1, VDC2 and VDC4)
	BLOCK

    ; Print Message
	LXI	H,MS_DATA   ; Values
	LXI	D,#03000	; Text RAM Address
	LXI	B,#01FF
	; Here C = 255
LOOP1:
    BLOCK           ; Copy 256 bytes
	DCR	B           ; B = B -1 skip next instruction on Borrow
	JR	LOOP1

    MVI A,#00       ; A = 0
LOOP2:
    MOV #03403, A   ; Set VDC3 value (bits 7-4: text color / bits 3-0: background color)
    INR A           ; A = A+ 1 skip next instruction on Carry
    NOP

    ; Wait
    MVI B,#FF
    MVI C,#FF
LOOP3:
    BIT 0,V.#FF     ; 10 thicks
    NOP
    DCR C
    JR LOOP3
    DCR B
    JR LOOP3

	JR	LOOP2       ; color cycling (infinite loop)

ORG #8040
VDC0:
	DB #C0, #00, #00

ORG	#8060
MS_DATA:    ; Message data (#3000-#31FF)
    DB '12345678911111111112222222222333'
    DB '2        01234567890123456789012'
    DB '3  ',#13,'                      ',#13,#13,'    '
    DB '4        Hello World!           '
    DB '5   I\'m Super Cassette Vision   '
    DB '6       and I\'m alive again     '
    DB '7                               '
    DB '8 CHAR 0123456789ABCDEF  012-DEF'
    DB '9    0x',#00,#01,#02,#03,#04,#05,#06,#07,#08,#09,#0A,#0B,#0C,#0D,#0E,#0F,' 8',#80,#81,#82,' ',#8D,#8E,#8F
    DB '10   1x',#10,#11,#12,#13,#14,#15,#16,#17,#18,#19,#1A,#1B,#1C,#1D,#1E,#1F,' 9',#90,#91,#92,' ',#9D,#9E,#9F
    DB '11   2x',#20,#21,#22,#23,#24,#25,#26,#27,#28,#29,#2A,#2B,#2C,#2D,#2E,#2F,' -       '
    DB '12   3x',#30,#31,#32,#33,#34,#35,#36,#37,#38,#39,#3A,#3B,#3C,#3D,#3E,#3F,' E',#E0,#E1,#E2,' ',#ED,#EE,#EF
    DB '13   4x',#40,#41,#42,#43,#44,#45,#46,#47,#48,#49,#4A,#4B,#4C,#4D,#4E,#4F,' F',#F0,#F1,#F2,' ',#FD,#FE,#FF
    DB '14   5x',#50,#51,#52,#53,#54,#55,#56,#57,#58,#59,#5A,#5B,#5C,#5D,#5E,#5F,'         '
    DB '15   6x',#60,#61,#62,#63,#64,#65,#66,#67,#68,#69,#6A,#6B,#6C,#6D,#6E,#6F,'         '
    DB '16 ',#13,' 7x',#70,#71,#72,#73,#74,#75,#76,#77,#78,#79,#7A,#7B,#7C,#7D,#7E,#7F,'   ',#13,#13,'    '

 ORG #BFFF
    DB #FF